 $(document).ready(function () {
     jQuery(function ($) {
         $('#accordion-des li').hover(function () {
             $(this).find('ul').stop(true, true).slideToggle()
         }).find('ul').hide()
     });

     if (!$('#myCanvas').tagcanvas({
             textColour: '#ffffff',
             outlineColour: 'rgba(255, 255, 255, 0.46)',
             reverse: true,
             depth: 0.8,
             maxSpeed: 0.05
         }, 'tags')) {
         // something went wrong, hide the canvas container DD
         $('#myCanvasContainer').hide();
     }
 });





