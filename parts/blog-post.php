
    <?php
        if (empty($_GET)) {
            $result = $mysqli->query('select articles.categorie_id AS catid ,articles.id AS artid,articles.title AS title, articles.image AS imgart , SUBSTRING(articles.text, 1, 500) AS text, articles_categories.title AS titlecat, articles.pubdate AS date,articles_categories.image AS image from articles JOIN articles_categories WHERE articles.categorie_id = articles_categories.id LIMIT 3');
        }else{
            $stmt = $mysqli->prepare('select articles.categorie_id AS catid, articles.id AS artid, articles.title AS title, articles.image AS imgart , SUBSTRING(articles.text, 1, 500) AS text, articles_categories.title AS titlecat, articles.pubdate AS date,articles_categories.image AS image from articles JOIN articles_categories WHERE articles.categorie_id = articles_categories.id AND articles_categories.id = ? LIMIT 3');
            $stmt->bind_param("i",$id);
            $id = $_GET['catid'];
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->free_result();
            $stmt->close();
        }
        while ($art = $result->fetch_assoc()) {
     ?>
    <div class="blog-classic">
        <div class="left-side">
            <?php
                $data = date_create($art['date']);
            ?>
            <div class="date">
                <?php echo date_format($data, 'd'); ?>
                <span><?php echo strtoupper(date_format($data, 'M'))." ".date_format($data, 'Y'); ?></span>
            </div>
            <div class="type">
                <img src="<?php echo $art['image']; ?>" alt="<?php echo $art['titlecat']; ?>" />
            </div>
        </div>
        <div class="blog-post">
            <div class="full-width">
                <img src="<?php echo $art['imgart']; ?>" alt="<?php echo $art['titlecat']; ?>" class="img-responsive" />
            </div>
            <h4 class="text-uppercase"><a href="../article.php?id=<?php echo $art['artid']; ?>"><?php echo $art['title']; ?></a></h4>
            <ul class="post-meta">
                <li><i class="fa fa-user"></i>posted by <a href="../resume.php">Yevgeniy Bespal</a></li>
                <li><i class="fa fa-folder-open"></i> <a href="/releases.php?catid=<?php echo $art['catid']; ?>"><?php echo $art['titlecat']; ?></a></li>
                <!--li><i class="fa fa-comments"></i> <a href="#">4 comments</a></li-->
            </ul>
            <p><?php
                if (strlen($art['text']) >= 500){
                    echo $art['text'] . "...";
                }else {
                    echo $art['text'];
                }
                ?></p>
            <a href="../article.php?id=<?php echo $art['artid']; ?>" class="btn btn-small btn-dark-solid  "> Continue Reading</a>
        </div>
    </div>
    <?php } $result->close(); ?>
    <!--classic gallery post-->


    <!--classic video post-->
    <!--<div class="blog-classic">
        <div class="left-side">
            <div class="date">
                24
                <span>MAR 2015</span>
            </div>
            <div class="type">
                <img src="img/type-post/github.png" alt="Android" />
            </div>
        </div>
        <div class="blog-post">
            <p class="video-fit m-bot-50 embed-responsive embed-responsive-16by9">
                <iframe width="560" height="315" src="//www.youtube.com/embed/Kf8G9AISsD4" allowfullscreen></iframe>
            </p>
            <h4 class="text-uppercase"><a href="blog-single.html">Video post</a></h4>
            <ul class="post-meta">
                <li><i class="fa fa-user"></i>posted by <a href="#">admin</a></li>
                <li><i class="fa fa-folder-open"></i> <a href="#">lifestyle</a>, <a href="#">travel</a>, <a href="#">fashion</a></li>
                <li><i class="fa fa-comments"></i> <a href="#">4 comments</a></li>
            </ul>
            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
            <a href="blog-single.html" class="btn btn-small btn-dark-solid  "> Continue Reading</a>
        </div>

    </div> -->
    <!--classic video post-->

    <!--classic image gallery post-->
    <!--<div class="blog-classic">
        <div class="left-side">
            <div class="date">
                24
                <span>MAR 2015</span>
            </div>
            <div class="type">
                <img src="img/type-post/ios.png" alt="Android" />
            </div>
        </div>
        <div class="blog-post">
            <div class="full-width">
                <img src="img/pluma.jpg" alt="" />
            </div>
            <h4 class="text-uppercase"><a href="blog-single.html">gallery post</a></h4>
            <ul class="post-meta">
                <li><i class="fa fa-user"></i>posted by <a href="#">admin</a></li>
                <li><i class="fa fa-folder-open"></i> <a href="#">lifestyle</a>, <a href="#">travel</a>, <a href="#">fashion</a></li>
                <li><i class="fa fa-comments"></i> <a href="#">4 comments</a></li>
            </ul>
            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
            <a href="blog-single.html" class="btn btn-small btn-dark-solid  "> Continue Reading</a>
        </div>
    </div>-->
    <!--classic image gallery post-->

    <!--classic post without image-->
    <!--<div class="blog-classic">
        <div class="left-side">
            <div class="date">
                24
                <span>MAR 2015</span>
            </div>
            <div class="type">
                <img src="img/type-post/OSX.png" alt="Android" />
            </div>
        </div>
        <div class="blog-post">
            <h4 class="text-uppercase"><a href="blog-single.html">blog post without photo</a></h4>
            <ul class="post-meta">
                <li><i class="fa fa-user"></i>posted by <a href="#">admin</a></li>
                <li><i class="fa fa-folder-open"></i> <a href="#">lifestyle</a>, <a href="#">travel</a>, <a href="#">fashion</a></li>
                <li><i class="fa fa-comments"></i> <a href="#">4 comments</a></li>
            </ul>
            <p>Lid est laborum dolo rumes fugats untras. Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.. <a>Sed ut perspiciatis unde</a> omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
            <a href="blog-single.html" class="btn btn-small btn-dark-solid  "> Continue Reading</a>
        </div>
    </div>-->
    <!--classic post without image-->
    <div class="buttons-nav">
        <ul>
            <li><a href="#">&#60; &nbsp; Previous</a></li>
            <?php for ($i = 1; $i<=5; $i++) { ?>
                <li>
                    <a href="#">
                        <?php echo($i) ?>
                    </a>
                </li>
            <?php } ?>
            <li><a href="#">Next &nbsp; &#62;</a></li>
        </ul>
    </div>
